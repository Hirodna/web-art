var canvas = document.getElementById('drawCanvas');
var ctx = canvas.getContext('2d');
var roughCanvas = rough.canvas(document.getElementById('drawCanvas'));
var mousedown= false;
var isDrawing = false;
var isErase=false;
var isRectangle=false;
var isEllipse=false;
var shapeTool=false;
var pencilTool=false;
var eraserTool=false;
var recTool = false;
var ellipseTool=false;
var x =0;
var y =0;



const rect = canvas.getBoundingClientRect();

canvas.addEventListener('mousedown', e => {
    x = e.clientX - rect.left;
    y = e.clientY - rect.top;
    last_mousex=x;
    last_mousey=y;
    mousedown=true;
  });

canvas.addEventListener('mousemove', e => {

  if (mousedown){
    if (!isErase)
    {
      ctx.globalCompositeOperation = 'source-over';
    }
    var colorFond =document.getElementById('colorFond').value;
    var colorContours=document.getElementById('colorContours').value;

    if (isDrawing === true && pencilTool===true) {
      drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
      x = e.clientX - rect.left;
      y = e.clientY - rect.top;
    }

    if (isErase === true && eraserTool===true) {
      eraseLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
      x = e.clientX - rect.left;
      y = e.clientY - rect.top;
    }

    if (isRectangle === true && recTool===true) {
      x = e.clientX - rect.left;
      y = e.clientY - rect.top;
      var width = x-last_mousex;
      var height = y-last_mousey;
      ctx.clearRect(last_mousex,last_mousey,width,height); //clear canvas

      roughCanvas.rectangle(last_mousex,last_mousey,width,height, {fill:colorFond, stroke: colorContours});

    }

    if (isEllipse === true && ellipseTool===true) {
      x = e.clientX - rect.left;
      y = e.clientY - rect.top;
      var width = x-last_mousex;
      var height = y-last_mousey;

      ctx.beginPath();
      roughCanvas.ellipse(last_mousex,last_mousey,width,height,{fill:colorFond, stroke: colorContours,});
      //ctx.arc(last_mousex,last_mousey,width,0,2*Math.PI);
      //ctx.globalCompositeOperation = 'source-in'
    }
  }

});




window.addEventListener('mouseup', e => {
    mousedown=false;
});

//dessine une ligne sur le passage
function drawLine(ctx, x1, y1, x2, y2) {
  var inputTaille = document.getElementById('inputTaille').value;
  var colorMine = document.getElementById('colorMine').value;
  ctx.beginPath();
  ctx.strokeStyle = colorMine;
  ctx.lineWidth = inputTaille;
  ctx.lineCap="round";
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
  ctx.closePath();
}


//gomme sur le passage
function eraseLine(context, x1, y1, x2, y2)
{
  var inputTailleGomme = document.getElementById('inputTailleGomme').value;
  ctx.beginPath();
  ctx.globalCompositeOperation = 'destination-out';
  ctx.lineWidth = inputTailleGomme;
  ctx.lineCap="round";
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
  ctx.closePath();
}




//ouvrir la fenetre des parametres outil associé
function openParam()
{
  if (pencilTool===true)
  {
    paramPen.style.display='block';
  }

  if (eraserTool===true)
  {
    paramEraser.style.display='block';
  }

  if (shapeTool===true)
  {
    paramShape.style.display='block';
  }

}

function closeParam()
{
  if (pencilTool===false)
  {
    paramPen.style.display='none';
  }

  if (eraserTool===false)
  {
    paramEraser.style.display ='none';
  }

  if (shapeTool===false)
  {
    paramShape.style.display='none';
  }
}

//Selection de l'outil crayon
var pencil = document.getElementById('pencil');
pencil.addEventListener('onclick',function(){selectPencil()});

function selectPencil()
{
  var paramPen = document.getElementById('paramPen');
  isErase=false;
  eraserTool=false;
  recTool=false;
  shapeTool=false;
  ellipseTool=false;
  pencilTool= true;
  isDrawing = true;
  openParam();
  closeParam();
}

//Selection de l'outil gomme
var eraser = document.getElementById('eraser');
eraser.addEventListener('onclick',function(){selectEraser()});

function selectEraser()
{
  var paramEraser = document.getElementById('paramEraser');
  isDrawing=false;
  pencilTool=false;
  recTool=false;
  shapeTool=false;
  ellipseTool=false;
  eraserTool= true;
  isErase = true;
  openParam();
  closeParam();
}

//Selection de l'outil forme
var shape=document.getElementById('shape')
shape.addEventListener('onclick', function(){selectShape()});

function selectShape()
{
  isErase = false;
  pencilTool=false;
  eraserTool=false;
  recTool=false;
  ellipseTool=false;
  shapeTool=true;
  openParam();
  closeParam();
}


//Selection de l'outil rectangle
var btn_rec=document.getElementById('btn_rec')
btn_rec.addEventListener('onclick', function(){selectRec()});

function selectRec()
{
  isErase=false;
  pencilTool=false;
  eraseTool=false;
  ellipseTool=false;
  recTool=true;
  isRectangle=true;
  openParam();
  closeParam();
}

var btn_ellipse=document.getElementById('btn_ellipse')
btn_ellipse.addEventListener('onclick', function(){selectEllipse()});

function selectEllipse()
{
  pencilTool=false;
  eraseTool=false;
  recTool=false;
  ellipseTool=true;
  isEllipse=true;
  openParam();
  closeParam();
}

// Sauvegarder son dessin

var btn_save = document.getElementById('btn_save');
btn_save.addEventListener('click',function()
{
  canvas.toBlob(function (blob)
  {
    const a = document.createElement('a')
    const timeStamp = Date.now().toString();
    document.body.appendChild(a);
    a.download='export-timeStamp.png';
    a.href= URL.createObjectURL(blob);
    a.click();
    a.remove();
   });
 });

// ouvrir un dessin

var btn_open = document.getElementById('btn_open');
btn_open.addEventListener('click', function()
{
  var winOpen =document.getElementById('openWinOpen');
  winOpen.style.display='block';
});

var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e){
  var reader = new FileReader();
  reader.onload = function(event)
  {
      var img = new Image();
      img.onload = function()
      {
          canvas.width = img.width;
          canvas.height = img.height;
          ctx.drawImage(img,0,0);
      }
      img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);
  var winOpen =document.getElementById('openWinOpen');
  winOpen.style.display='none';
}

function closeOpenWin()
{
  var winOpen =document.getElementById('openWinOpen');
  winOpen.style.display='none';
}


//nouveau document
function newCanvas()
{
  ctx.clearRect(0,0,canvas.width,canvas.height);
}



//zoomer
function zoomin()
{
    var DC = document.getElementById("drawCanvas");
    var currWidth = DC.clientWidth;
    DC.style.width = (currWidth + 100) + "px";
}

//dezoomer
function zoomout()
{
    var DC = document.getElementById("drawCanvas");
    var currWidth = DC.clientWidth;
    DC.style.width = (currWidth - 100) + "px";
}
